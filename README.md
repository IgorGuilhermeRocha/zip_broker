# ZIP BROKER

## About

This is a simple project, aimed at finding the password for a zip file and sending an email containing the file's content and the password.

## Usage

In the root folder, type in your terminal:

```bash
python3 zip_broker.py -f mal.zip -d dicionario.txt
```

If the program manages to find the password for the zip file, it will create a folder with the content of that file and send it via email along with the found password.

You can change both, the zip file and the dictionary. 

## credentials.json
The credentials.json was not commited, because this file have cofidential information.
Below, I will show how the file should be.

```json
{   
    "smtp_server": "smtp-mail.outlook.com",
    "smtp_port": 587,
    "sender_email": "test23@hotmail.com",
    "sender_password": "Be water my friend"
}
```

### SMTP Server and port
This information is simple to find, just search in the internet **What is my SMTP server?**, 
google, outlook and others have a different smtp server and port.

### Sender email and password
This normally will be your email and your password.
